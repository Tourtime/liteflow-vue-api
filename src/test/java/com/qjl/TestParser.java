package com.qjl;

import cn.hutool.core.io.resource.ResourceUtil;
import com.qjl.core.ELNode;
import com.qjl.core.ExpressLanguageParseException;
import com.qjl.parser.ELParser;
import com.qjl.parser.logicflow.LogicFlow;
import com.qjl.parser.logicflow.LogicFlowParser;
import com.qjl.util.Json2ELNodeParser;
import org.junit.Test;

/**
 * @author : zhangrongyan
 * @date : 2023/1/13 14:02
 */
public class TestParser {
    public static void doWithElNodeJson(String path) throws ExpressLanguageParseException {
        String json = ResourceUtil.readUtf8Str(path);
        Json2ELNodeParser json2ELNodeParser = new Json2ELNodeParser(json);
        ELNode el = json2ELNodeParser.parse();
        String elStr = el.generateEl();
        System.out.println(elStr);
    }

    public static void doWithLogicFlowJson(String path) {
        String json = ResourceUtil.readUtf8Str(path);
        Json2ELNodeParser json2ELNodeParser = new Json2ELNodeParser(json);
        LogicFlow logicFlow = json2ELNodeParser.parseLogicFlow();
        ELParser parser = new LogicFlowParser(logicFlow);
        ELNode elNodeFromLogicFlowData = parser.extractElNode();
        try {
            System.out.println(elNodeFromLogicFlowData.generateEl());
        } catch (ExpressLanguageParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSimple() throws ExpressLanguageParseException {
        doWithElNodeJson("json/simple.json");

    }

    @Test
    public void testPublicActivity() throws ExpressLanguageParseException {
        doWithElNodeJson("json/publishActivity2.json");

    }

    @Test
    public void testSimpleWhen() throws ExpressLanguageParseException {
        doWithElNodeJson("json/simpleWhen.json");

    }

    @Test
    public void testLogicFlowWhenDataToElNode() throws ExpressLanguageParseException {
        doWithLogicFlowJson("json/logicFlowWhenData2.json");
    }

    @Test
    public void testLogicFlowIfDataToElNode() throws ExpressLanguageParseException {
        doWithLogicFlowJson("json/logicFlowIfData.json");
    }

    @Test
    public void testLogicFlowBugDataToElNode() throws ExpressLanguageParseException {
        doWithLogicFlowJson("json/bug0424.json");
    }

    @Test
    public void testLogicFlowBugDataToElNode0427() throws ExpressLanguageParseException {
        doWithLogicFlowJson("json/bug0427.json");
    }
}

package com.qjl.pojo.chain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : zhangrongyan
 * @date : 2023/1/13 10:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicActivityRuleRequestData {
    private Long ghId;
}

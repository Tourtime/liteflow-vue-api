package com.qjl.pojo.context;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author : zhangrongyan
 * @date : 2023/1/12 15:39
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderContext {
    private String orderNo;
}

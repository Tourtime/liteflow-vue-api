package com.qjl.controller;

import com.qjl.core.ELNode;
import com.qjl.core.ExpressLanguageParseException;
import com.qjl.parser.ELParser;
import com.qjl.parser.logicflow.LogicFlow;
import com.qjl.parser.logicflow.LogicFlowParser;
import com.qjl.service.ApiService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "我的接口")
@RequestMapping("/api")
@RestController
public class ApiController {


    @Autowired
    ApiService apiService;

    @ApiOperation(value = "ok",notes = "测试服务是否存活")
    @GetMapping("/ok")
    public String ok() {
        apiService.ok();
        return "success";
    }



    @ApiOperation(value = "转化el表达式", notes = "输入ElNode结构")
    @ApiImplicitParam(name = "elNode", value = "节点", required = true)
    @PostMapping("/generateEL")
    public String generateEL(@RequestBody ELNode elNode) {
        String sqlTemplate = null;
        try {
            sqlTemplate = elNode.generateEl();
        } catch (ExpressLanguageParseException e) {
            e.printStackTrace();
        }
        return sqlTemplate;
    }

    @ApiOperation(value = "logicFlow转化el表达式", notes = "输入logicFlow 结构")
    @ApiImplicitParam(name = "logicFlow", value = "节点", required = true)
    @PostMapping("/generateLogicFlowEL")
    public String generateLogicFlowEL(@RequestBody LogicFlow logicFlow) {
        String sqlTemplate = null;
        try {
            ELParser elParser = new LogicFlowParser(logicFlow);
            ELNode elNode = elParser.extractElNode();
            sqlTemplate = elNode.generateEl();
        } catch (ExpressLanguageParseException e) {
            e.printStackTrace();
        }
        return sqlTemplate;
    }

}

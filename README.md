# Liteflow-vue 的后端演示项目


## 接收LogicFlow的数据结构
- 接收logicFlow前端的json，转化el表达式,演示接口：/api/generateLogicFlowEL

- 效果：
  ![](doc/lfimg.png)

## 接收ELNode 数据结构
- 接收前端的json，转化el表达式,演示接口：/api/generateEL

- 效果：
![](doc/img.jpg)


![skimg](doc/sk.jpg)